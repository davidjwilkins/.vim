set nocompatible                "be iMproved, required
filetype off

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'ap/vim-css-color'
Plugin 'bling/vim-airline'
Plugin 'dericofilho/vim-phpfmt'
Plugin 'EinfachToll/DidYouMean'
Plugin 'evidens/vim-twig'
Plugin 'fatih/vim-go'
Plugin 'godlygeek/tabular'
Plugin 'honza/vim-snippets'
Plugin 'jbgutierrez/vim-partial'
Plugin 'kien/ctrlp.vim'
Plugin 'kien/rainbow_parentheses.vim'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'majutsushi/tagbar'
Plugin 'mhinz/vim-startify'
Plugin 'mileszs/ack.vim'
Plugin 'pangloss/vim-javascript'
Plugin 'Raimondi/delimitMate'
Plugin 'rking/ag.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'SirVer/ultisnips'
Plugin 'talek/obvious-resize'
"vim hard mode:
"Plugin 'takac/vim-hardtime'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'tomasr/molokai'
Plugin 'tpope/vim-abolish'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-projectionist'
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-speeddating'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-unimpaired'
Plugin 'Valloric/YouCompleteMe'
Plugin 'Yggdroot/indentLine'

call vundle#end()            " required

filetype plugin indent on    " required
let g:mapleader=","

" ---PHP---
let g:phpfmt_on_save = get(g:, 'phpfmt_on_save', 1) " format on save (autocmd)
let g:phpfmt_php_path = "php"               " Path to PHP
let g:phpfmt_enable_default_mapping = 1     " Enable the mapping by default (<leader>pcd)
"reindent/reformat php file
au FileType php nnoremap <F1> <leader>pcf
" Toggle paste mode on and off
map <F2> :setlocal paste!<cr>
imap <F2> <c-o>:setlocal paste!<cr>
map <F3> :setlocal paste!<cr>
imap <F3> <c-o>:setlocal spell!<cr>
nmap <F4> :RainbowParenthesesToggle
"make it easy to copy multi line text to clipboard
nmap <F5> :set norelativenumber<CR>:set nu!<CR>:NERDTreeToggle<CR>:GitGutterToggle<CR><c-w><c-l>
"toggle relative number
nmap <F6> :set rnu!<CR>
nmap <F7> :NERDTreeToggle<CR>
nmap <F8> :TagbarToggle<CR>
"align by : or by = in visual mode
vmap <Leader>: :Tabularize /:
vmap <Leader>= :Tabularize /=
syntax on
"sudo save
cmap w!! w !sudo tee % >/dev/null
vmap <Leader>x :PartialExtract<cr>
let g:partial_templates = {
      \   'twig': '{% include %s %}',
      \ }
"---EASY MOTION---
",s to begin an easysearch
"s to do a two character search, place cursor on word
"t to do a two character search, place cursor before word
"case insensitive unless you use a capital
map <Leader> <Plug>(easymotion-prefix)
nmap s <Plug>(easymotion-s2)
nmap t <Plug>(easymotion-t2)
let g:EasyMotion_smartcase = 1
let g:EasyMotion_use_smartsign_us = 1
set laststatus=2
let g:airline_powerline_fonts = 1
"<c-p> finds nearest ancestor with a .git file as root
let g:ctrlp_working_path_mode = 'ra'
"disable screen flashing
set vb
set t_vb=
set noerrorbells
set novisualbell
set omnifunc=syntaxcomplete#Complete
"last line should have an end of line
set eol
set encoding=utf-8 nobomb
scriptencoding utf-8 nobomb
set background=dark
set backspace=indent,eol,start
"show match for partly types search command
set incsearch
"ignore case when using a search pattern
set ignorecase
"override 'ignorecase' when pattern has upper case characters
set smartcase
set showmatch 
"highlight all matches for the last used search pattern
set hlsearch
set wildmenu
set wildmode=list:longest,full
"don't redraw while executing macros
set lazyredraw
"change the way backslashes are used in search patters
set magic
set history=1000 
"minimm number of lines to scroll at a time
set scrolljump=5
set virtualedit=onemore
"show the line number for each line
set number
"highlight the row the cursor is on
set cursorline
set autoread
set noswapfile nobackup nowb
set autoindent smartindent smarttab expandtab
"number of spaces after a tab
set shiftwidth=4 softtabstop=4 tabstop=4
"number of screen lines to show around the cursor
set scrolloff=1
set showcmd
"long lines wrap
set wrap
set ai si wrap
if has('persistent_undo')
    silent !mkdir ~/.vim/backups > /dev/null 2>&1
    set undodir=~/.vim/backups
    set undofile
endif
"display tabs and trailing spaces
set list listchars=tab:\ \ ,trail:·
nnoremap j gj
nnoremap k gk
" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l
" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm
cmap Tabe tabe
cmap cwd lcd %:p:h
cmap cd. lcd %:p:h
nnoremap <leader>s :mksession<CR>
nnoremap <leader>a :Ag
noremap <silent> <C-Up> :ObviousResizeUp<CR>
noremap <silent> <C-Down> :ObviousResizeDown<CR>
noremap <silent> <C-Left> :ObviousResizeLeft<CR>
noremap <silent> <C-Right> :ObviousResizeRight<CR>
let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

"Wrap lines at convenient points
set linebreak
set foldmethod=syntax foldnestmax=10 foldlevelstart=10 foldenable
nnoremap <space> za
imap kj <Esc>
cmap w!! w !sudo tee % >/dev/null
"highlight column 120 if vim supports it
if has('colorcolumn')
    set colorcolumn=120
endif
"open nerdtree when vim starts, move cursor to editor
autocmd VimEnter * NERDTree | wincmd p
autocmd VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces
"close vim if the only thing left open in a nerdtree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
nnoremap <CR> :noh<CR><CR>:<backspace>
filetype plugin indent on    " required
let g:molokai_original = 1
set t_Co=256
let g:rehash256 = 1
colorscheme molokai
" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>
au FileType go nmap <Leader>s <Plug>(go-implements)
au FileType go nmap <Leader>i <Plug>(go-info)
au FileType go nmap <Leader>gd <Plug>(go-doc)
au FileType go nmap <Leader>gv <Plug>(go-doc-vertical)
au FileType go nmap <Leader>gb <Plug>(go-doc-browser)
au FileType go nmap <leader>r <Plug>(go-run)
au FileType go nmap <leader>b <Plug>(go-build)
au FileType go nmap <leader>t <Plug>(go-test)
au FileType go nmap <leader>c <Plug>(go-coverage)
au FileType go nmap <Leader>ds <Plug>(go-def-split)
au FileType go nmap <Leader>dv <Plug>(go-def-vertical)
au FileType go nmap <Leader>dt <Plug>(go-def-tab)
au FileType go nmap <Leader>e <Plug>(go-rename)
let g:go_fmt_command = "goimports"
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
autocmd FileType go autocmd BufWritePre <buffer> Fmt
autocmd FileType go compiler go

