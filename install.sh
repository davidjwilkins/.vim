#!/bin/bash
git submodule update --init --recursive
vim +PluginInstall +qall
while true; do
    read -p "Do you wish to install fonts? " yn
    case $yn in
        [Yy]* ) bash ./fonts/install.sh; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done
while true; do
    read -p "Symlink .vimrc? " yn
    case $yn in
        [Yy]* ) rm ~/.vimrc && ln -s ~/.vim/.vimrc ~/.vimrc; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done
while true; do
    read -p "Compile YouCompleteMe? " yn
    case $yn in
        [Yy]* ) cd ~/.vim/bundle/YouCompleteMe && ./install.sh; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done
